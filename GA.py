import numpy as np
import pandas as pd
import random
from SNN import SNN
import time


class GA(object):
    def __init__(self, n, nExcitatory, networks, generations, delta):
        self.n = n
        self.nExcitatory = nExcitatory
        self.nInhibitory = n - nExcitatory
        self.networks = networks
        self.generations = generations
        self.delta = delta
        self.population = self.InitializePopulation()
        self.elite = int(0.04*len(self.population))
        self.mutants = int(0.04*len(self.population))
        self.Simulation = SNN(self.Transform(), self.networks)
        self.positions = range(-50, 50, 4)
        self.deltaAverage = 0
        self.previousAverage = 0
        self.previousMax = 0
        self.margin = 1


    def InitializePopulation(self):
        fitness = 0.5
        population = list()
        for i in range(self.networks):
            A = np.array([-1 for i in range(self.nExcitatory)] + [random.uniform(-1,1) for i in range(self.nInhibitory)], dtype='float64')
            B = np.array([-1 for i in range(self.nExcitatory)] + [random.uniform(-1,1) for i in range(self.nInhibitory)], dtype='float64')
            C = np.array([random.uniform(-1,1) for i in range(self.nExcitatory)] + [-1 for i in range(self.nInhibitory)], dtype='float64')
            D = np.array([random.uniform(-1,1) for i in range(self.nExcitatory)] + [-1 for i in range(self.nInhibitory)], dtype='float64')
            Ws = np.array([[random.uniform(-1,1) for j in range(7)] for i in range(self.n)], dtype='float32')
            Wi = np.array([[random.uniform(0,1) for j in range(self.nExcitatory)] + [random.uniform(-1,0) for j in range(self.nInhibitory)] for i in range(self.n)], dtype='float32')
            Wm = np.array([[random.uniform(0,1) for i in range(self.nExcitatory)] + [random.uniform(-1,0) for i in range(self.nInhibitory)] for i in range(2)], dtype='float32')
            ts = random.uniform(-1,1)
            tm = random.uniform(-1,1)
            Bs = random.uniform(-1,1)
            Bi = np.array([random.uniform(-1,1) for i in range(self.n)], dtype='float64')
            Bm = np.array([random.uniform(-1,1) for i in range(2)], dtype='float64')
            population.append([fitness, self.n, A, B, C, D, Ws, Wi, Wm, ts, tm, Bs, Bi, Bm])
        
        return population


    def Transform(self):
        transform = [[] for i in range(14)]
        transform[1] = self.n
        for individual in self.population:
            transform[0].append(0.5)
            for i in range(2, 14):
                transform[i].append(individual[i])

        result = list()
        for element in transform:
            result.append(np.array(element))
        parameters = np.array(result)
    
        return parameters


    def GetFitness(self):
        testFitness = list()
        for testType in [True, False]:
            for i in range(24):
                self.Simulation.Reset()
                p = self.Simulation.Run(self.delta, self.positions[i], testType)
                print('|', end='', flush=True)
                testFitness.append(p)
        testFitness = np.array(testFitness)
        fitness = np.sum(testFitness, axis=0) / 48.0
        for i, individual in enumerate(self.population):
            individual[0] = fitness[i]
        fitnessAverage = np.sum(fitness) / len(self.population)
        fitnessMax = max(fitness)
        self.deltaAverage = fitnessAverage - self.previousAverage
        deltaFitness = fitnessMax - self.previousMax
        self.margin = fitnessMax - fitnessAverage
        self.previousAverage = fitnessAverage
        self.previousMax = fitnessMax

        print('')
        print("    Generation's average: ", 100 * fitnessAverage, end=' | ')
        print('Delta: ', 100 * self.deltaAverage)
        print("    Generation's best: ", 100 * fitnessMax, end=' | ')
        print('Delta: ', 100 * deltaFitness)
        print('    Margin: ', 100 * self.margin)


    def Selection(self):
        parents = list()
        for i in range(2):
            p = random.uniform(0,1)
            index1 = random.randint(0, len(self.population)-1)
            index2 = random.randint(0, len(self.population)-1)
            contenders = [self.population[index1][0], self.population[index2][0]]
            indexMax = contenders.index(max(contenders))
            if p < 0.95:
                if indexMax == 0:
                    parents.append(self.population[index1])
                else:
                    parents.append(self.population[index2])
            else:
                if indexMax == 1:
                    parents.append(self.population[index1])
                else:
                    parents.append(self.population[index2])

        return parents


    def Crossover(self):
        if self.deltaAverage > 0:
            mask = np.array([random.randint(0, 1) for i in range(n)])
            notMask = np.logical_not(mask)
            WsMask = np.array([[random.randint(0,1) for j in range(7)] for i in range(self.n)], dtype='float32')
            notWsMask = np.array([np.logical_not(row) for row in WsMask], dtype='float32')
            WiMask = np.array([[random.randint(0,1) for j in range(self.n)] for i in range(self.n)], dtype='float32')
            notWiMask =  np.array([np.logical_not(row) for row in WiMask], dtype='float32')
            WmMask = np.array([[random.randint(0,1) for i in range(self.n)] for i in range(2)], dtype='float32')
            notWmMask = np.array([np.logical_not(row) for row in WmMask], dtype='float32')
            ts = random.randint(0,1)
            tm = random.randint(0,1)
            Bs = random.randint(0,1)
            Bi = np.array([random.randint(0,1) for i in range(self.n)], dtype='float64')
            Bm = np.array([random.randint(0,1) for i in range(2)])
            notBm = np.logical_not(Bm)

        else:
            mask = np.array([random.randint(0,1) for i in range(n)])
            notMask = np.logical_not(mask)
            WsMask = np.array([random.randint(0,1) for i in range(self.n)], dtype='float32')[:,None]
            notWsMask = np.logical_not(WsMask)
            WiMask = np.array([random.randint(0,1) for i in range(self.n)], dtype='float32')[:,None]
            notWiMask =  np.logical_not(WiMask)
            WmMask = np.array([random.randint(0,1) for i in range(2)], dtype='float32')[:,None]
            notWmMask = np.logical_not(WmMask)
            ts = random.randint(0,1)
            tm = random.randint(0,1)
            Bs = random.randint(0,1)
            Bi = np.array([random.randint(0,1) for i in range(self.n)], dtype='float64')
            Bm = np.array([random.randint(0,1) for i in range(2)])
            notBm = np.logical_not(Bm)

        offspring = self.population[:self.elite]
        for i in range(int(len(self.population[self.elite:]) / 2)):
            parent1, parent2 = self.Selection()
            child1 = list()
            child2 = list()
            # Child 1
            child1.append(0.5)
            child1.append(self.n)
            for j in range(4):
                child1.append(parent1[j+2] * mask + parent2[j+2] * notMask)
            child1.append(np.multiply(parent1[6], WsMask) + np.multiply(parent2[6], notWsMask))  
            child1.append(np.multiply(parent1[7], WiMask) + np.multiply(parent2[7], notWiMask))
            child1.append(np.multiply(parent1[8], WmMask) + np.multiply(parent2[8], notWmMask))
            for element in parent1[9:12]:
                child1.append(element)
            child1.append(parent1[12] * mask + parent2[12] * notMask)
            child1.append(parent1[13] * Bm + parent2[13] * notBm)
            # Child 2
            child2.append(0.5)
            child2.append(self.n) 
            for j in range(4):
                child2.append(parent2[j+2] * mask + parent1[j+2] * notMask)
            child2.append(np.multiply(parent2[6], WsMask) + np.multiply(parent1[6], notWsMask)) 
            child2.append(np.multiply(parent2[7], WiMask) + np.multiply(parent1[7], notWiMask))
            child2.append(np.multiply(parent2[8], WmMask) + np.multiply(parent1[8], notWmMask))    
            for element in parent2[9:12]:
                child2.append(element)
            child2.append(parent2[12] * mask + parent1[12] * notMask)
            child2.append(parent2[13] * Bm + parent1[13] * notBm)
            # Add both to new population
            offspring.append(child1)
            offspring.append(child2)

        self.population = offspring


    def Mutation(self):
        for i in range(self.mutants):
            A = np.array([-1 for i in range(self.nExcitatory)] + [random.uniform(-1,1) for i in range(self.nInhibitory)], dtype='float64')
            B = np.array([-1 for i in range(self.nExcitatory)] + [random.uniform(-1,1) for i in range(self.nInhibitory)], dtype='float64')
            C = np.array([random.uniform(-1,1) for i in range(self.nExcitatory)] + [-1 for i in range(self.nInhibitory)], dtype='float64')
            D = np.array([random.uniform(-1,1) for i in range(self.nExcitatory)] + [-1 for i in range(self.nInhibitory)], dtype='float64')
            Ws = np.array([[random.uniform(-1,1) for j in range(7)] for i in range(self.n)], dtype='float32')
            Wi = np.array([[random.uniform(0,1) for j in range(self.nExcitatory)] + [random.uniform(-1,0) for j in range(self.nInhibitory)] for i in range(self.n)], dtype='float32')
            Wm = np.array([[random.uniform(0,1) for i in range(self.nExcitatory)] + [random.uniform(-1,0) for i in range(self.nInhibitory)] for i in range(2)], dtype='float32')
            ts = random.uniform(-1,1)
            tm = random.uniform(-1,1)
            Bs = random.uniform(-1,1)
            Bi = np.array([random.uniform(-1,1) for i in range(self.n)], dtype='float64')
            Bm = np.array([random.uniform(-1,1) for i in range(2)], dtype='float64')

            newValues = np.array([0.5, self.n, A, B, C, D, Ws, Wi, Wm, ts, tm, Bs, Bi, Bm])
            mask = np.array([0 for i in range(14)])
            mask[random.randint(2,13)] = 1
            j = random.randint(self.elite, len(self.population) - 1)
            self.population[j] = self.population[j] * np.logical_not(mask) + self.population[j] * mask * newValues


    def SaveResult(self, generation, population):
        for parameters in population:
            fitness, n, A, B, C, D, Ws, Wi, Wm, ts, tm, Bs, Bi, Bm = parameters
            network = dict()
            network['Generation'] = [generation]
            network['Fitness'] = [fitness]
            for i, element in enumerate(A):
                key = 'A' + str(i+1)
                network[key] = [element]
            for i, element in enumerate(B):
                key = 'B' + str(i+1)
                network[key] = [element]
            for i, element in enumerate(C):
                key = 'C' + str(i+1)
                network[key] = [element]
            for i, element in enumerate(D):
                key = 'D' + str(i+1)
                network[key] = [element]
            for i, element in enumerate(Ws):
                for j, weight in enumerate(element):
                    key = 'Ws' + str(i+1) + str(j+1)
                    network[key] = [weight]
            for i, element in enumerate(Wi):
                for j, weight in enumerate(element):
                    key = 'Wi' + str(i+1) + str(j+1)
                    network[key] = [weight]
            for i, element in enumerate(Wm):
                for j, weight in enumerate(element):
                    key = 'Wm' + str(i+1) + str(j+1)
                    network[key] = [weight]
            network['ts'] = [ts]
            network['tm'] = [tm]
            network['Bs'] = [Bs]
            for i, element in enumerate(Bi):
                key = 'Bi' + str(i+1)
                network[key] = [element]
            for i, element in enumerate(Bm):
                key = 'Bm' + str(i+1)
                network[key] = [element]

            networkDF = pd.DataFrame(data=network)
            with open(str(self.nExcitatory) + '-' + str(n) + '_network.csv','a') as f:
                networkDF.to_csv(f, header=False, index=False)


    def Run(self):
        i = 1
        timeRecord = list()
        while i <= self.generations and self.margin > 0.001:
            startTime = time.time()
            print('Generation: ', i, end='  ')
            self.Simulation.__init__(self.Transform(), self.networks)
            self.GetFitness()
            self.population.sort(key=lambda x: x[0], reverse=True)
            self.SaveResult(i, self.population)
            self.Crossover()
            self.Mutation()
            timeRecord.append(time.time() - startTime)
            timeAverage = sum(timeRecord) / i
            timeProjection = (self.generations - i) * timeAverage
            print("    --- Time: {}min ({}sec) ETR: {}h ({}min)---".format(int(timeRecord[i-1]/60.0), int(timeRecord[i-1]), int(timeProjection/3600.0), int(timeProjection/60)))
            i += 1


n = 3
nExcitatory = 2
networks = 1000

generations = 160
delta = 0.1

startTime = time.time()

Simulation = GA(n, nExcitatory, networks, generations, delta)
Simulation.Run()

print("--- Combined time: %s seconds ---" % (time.time() - startTime))