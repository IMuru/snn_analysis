import numpy as np
import pandas as pd
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.axes as axes


class SNN(object):
    def __init__(self, parameters, networks):
        self.defaults = [parameters, networks]
        self.GPU_init = True
        fitness, n, A, B, C, D, Ws, Wi, Wm, ts, tm, Bs, Bi, Bm = parameters
        self.networks = networks
        self.n = n
        # Create sensory neurons
        # Parametrization 
        self.ts = 0.5*ts + 1.5
        self.Bs = Bs - 3
        self.SensoryOutput = np.array([[0.0 for j in range(7)] for i in range(networks)], dtype='float64')
        # Create spiking neurons
        # Parametrization of neurons' dynamics 
        self.A = 0.04*A + 0.06
        self.B = 0.025*B + 0.225
        self.C = 7.5*C - 57.5
        self.D = 3*D + 5
        # Parametrization of neurons' conections
        self.Ws = 50*Ws
        self.Wi = 50*Wi
        self.Bi = 4*Bi
        # Neurons' state variables
        self.I = np.array([[0.0 for j in range(self.n)] for i in range(networks)], dtype='float64')
        self.S = np.array([[0.0 for j in range(self.n)] for i in range(networks)], dtype='float64')
        self.V = np.array([[-65.0 for j in range(n)] for i in range(networks)], dtype='float64')
        self.U = self.B * self.V
        # Neurons' output
        self.NeuronOutput = np.array([[0.0 for j in range(self.n)] for i in range(networks)], dtype='float64') 
        # Create motor neurons
        # Parametrization
        self.tm = 0.5*tm + 1.5
        self.Wm = 50*Wm
        self.Bm = 4*Bm
        self.h = 5
        # Neuron's output
        self.history = np.array([[[0.0 for k in range(self.n)] for j in range(networks)] for i in range(self.h)], dtype='float64') 
        self.MotorOutput = np.array([[0.0 for j in range(2)] for i in range(networks)], dtype='float64') 

        # Enviroment initialization
        self.agentX = np.zeros(networks)
        self.rayEnd = np.array([213.0, 217.0, 219.0, 220.0, 219.0, 217.0,  213.0])
        self.rayEndSides = np.array([213.0, 217.0, 219.0, 219.0, 217.0,  213.0])
        self.theta = np.array([-0.2618, -0.1745, -0.0873, 0.0, 0.0873, 0.1745, 0.2618])
        self.thetaSides = np.array([-0.2618, -0.1745, -0.0873, 0.0873, 0.1745, 0.2618])
        self.testShape = np.array([0.0, 275.0])
        self.K = np.array([[0.0 for j in range(7)] for i in range(networks)], dtype='float64')
        self.finishFlag = False
        self.data = [[] for i in range(3)]
        self.collisionPoints = list()
        self.movementRecord = list()


    def Reset(self):
        parameters, networks = self.defaults
        self.__init__(parameters, networks)


    def Update(self, delta, GPU_init):
        # Calculate sensory input layer
        self.SensoryOutput = self.SensoryOutput + delta*((-self.SensoryOutput + self.K) / self.ts[:,None])
        # Calculate hidden layer
        self.S = np.sum(self.Ws / (1 + np.exp(-(self.SensoryOutput + self.Bs[:,None])))[:,None], axis=2)
        self.I = np.sum(self.Wi * self.NeuronOutput[:,None], axis=2)
        self.V = self.V + delta * (0.04*self.V**2 + 5*self.V + 140 - self.U + self.S + self.I)
        self.U = self.U + delta * (self.A * (self.B*self.V - self.U))
        self.NeuronOutput = (self.V >= 30) * 1
        self.V = self.V * np.logical_not(self.NeuronOutput) + self.C * self.NeuronOutput
        self.U = self.U * np.logical_not(self.NeuronOutput) + (self.U + self.D) * self.NeuronOutput
        # Calculate output layer
        self.history = np.roll(self.history, 1, axis=0)
        self.history[0] = self.NeuronOutput
        movingAverage = np.sum(self.history, axis=0) / self.h
        self.MotorOutput = self.MotorOutput + delta * ((-self.MotorOutput + np.sum(self.Wm * movingAverage[:,None], axis=2)) / self.tm[:,None])
        self.MotorOutput = self.MotorOutput * (self.MotorOutput > 0) * 1

        # Update every agent's velocity and position
        MotorOutputT = np.transpose(self.MotorOutput)
        velocityX = MotorOutputT[0] - MotorOutputT[1]
        self.agentX = self.agentX + delta * velocityX
        rightLimit = (self.agentX > 50) * 1
        leftLimit = (self.agentX < -50) * 1
        self.agentX = self.agentX * np.logical_not(rightLimit) * np.logical_not(leftLimit)+ (50 * rightLimit) - (50 * leftLimit)
        # Update test shape position
        self.testShape[1] = self.testShape[1] - 3.0 * delta
        # Get collision points and check if the test shape has reached the botom
        if self.testType:
            self.CircleCollision()
            if (self.testShape[1] - 1) <= 0:
                self.finishFlag = True
        else:
            self.LineCollision()
            if (self.testShape[1] - 16) <= 0:
                self.finishFlag = True

  
    def Run(self, delta, initialPosition, testType):
        self.testType = testType
        self.testShape[0] = initialPosition
        KData = list()
        VData = list()
        NOData = list()
        MOData = list()
    
        while(not self.finishFlag):
            self.Update(delta, self.GPU_init)
            self.GPU_init = False
            KData.append(self.K[0])
            VData.append(self.V[0])
            NOData.append(self.NeuronOutput[0])
            MOData.append(self.MotorOutput[0])
        self.data = [KData, VData, NOData, MOData]

        d = np.abs(self.agentX - self.testShape[0])
        dNormalized = d * (d < 45) + (d >= 45) * 45
        dNormalized = dNormalized / 45.0
       
        if self.testType:
            p = 1 - dNormalized
        
        else:
            p = dNormalized
        
        print(self.testShape[0], self.agentX[0], ' --> ', d)
        self.PlotData()

        return p, self.data[2], self.agentX


    def LineCollision(self):
        self.K = np.array([np.zeros(7) for i in range(self.networks)])
        # Calculate solution and images
        X = self.agentX[:, None] + self.theta * self.testShape[1]
        y = self.testShape[1]
        # Calculate distances to collision points 
        d = np.sqrt((X - self.agentX[:, None])**2 + y**2)
        # Generate mask based on constrains and filter valid results
        validCollisions = (self.rayEnd > y) * (X > self.testShape[0] - 15) * (X < self.testShape[0] + 15) * 1
        d = d * validCollisions
        d = d + 220 * (d == 0)
        self.K = 10 - (d / 22.0)
    

    def CircleCollision(self):
        self.K = np.array([np.zeros(7) for i in range(self.networks)])
        # Calculate ray 3
        X = self.agentX
        sqroot = 225 - (self.agentX - self.testShape[0])**2
        sqroot = sqroot * (sqroot > 0)
        Y = -np.sqrt(sqroot) + self.testShape[1]              
        # Calculate distances to collision point
        d3 = np.sqrt((X - self.agentX)**2 + Y**2)
        # Generate mask based on constrains and filter valid results
        validCollisions = (Y > 0) * (Y < self.rayEnd[3]) * 1
        d3 = d3 * validCollisions

        # ax^2 + bx + c = 0, where a, b and c are real numbers and a != 0
        constant = -self.agentX[:, None] / self.thetaSides - self.testShape[1]
        a = (1 + 1.0 / self.thetaSides**2)[None, :]
        b = 2 * (constant / self.thetaSides[None, :] - self.testShape[0])
        c = constant**2 - 225 + self.testShape[0]**2
        # calculate the discriminant
        disc = b**2 - 4*a*c
        # Calculate collision solutions
        disc = disc * (disc>0 * 1)
        X1 = (-b + np.sqrt(disc))/(2*a)
        X2 = (-b - np.sqrt(disc))/(2*a)
        # Calculate images based on line equation
        Y1 = (X1 - self.agentX[:, None]) / self.thetaSides[None, :]
        Y2 = (X2 - self.agentX[:, None]) / self.thetaSides[None, :]
        # Calculate distances to collision points
        d1 = np.sqrt((X1 - self.agentX[:, None])**2 + Y1**2)
        d2 = np.sqrt((X2 - self.agentX[:, None])**2 + Y2**2)
        # Generate masks based on constrains 
        validCollisions1 = (Y1 > 0) * (Y1 < self.rayEndSides[None, :]) * 1
        validCollisions2 = (Y2 > 0) * (Y2 < self.rayEndSides[None, :]) * 1
        # Filter valid results
        d1 = d1 * validCollisions1
        d2 = d2 * validCollisions2
        # Get minimum distaces
        conflict = validCollisions1 * validCollisions2
        noConflict = np.logical_not(conflict)
        dMin = np.minimum(d1 * conflict, d2 * conflict)
        d = dMin + d1 * noConflict + d2 * noConflict
        # Insert result for ray 3
        d = np.insert(d, [3], d3[:, None], axis=1)
        d = d + 220 * (d == 0)
        self.K = 10 - (d / 22.0)


    def TransformData(self):
        K = [[element[i] for element in self.data[0]] for i in range(7)]
        neuron = [[element[i] for element in self.data[1]] for i in range(3)]
        fired  = [[element[i] for element in self.data[2]] for i in range(3)]
        motorNeuron = [[(-1)**i * element[i] for element in self.data[3]] for i in range(2)]
        movement = [element[0] - element[1] for element in self.data[3]]
        
        return K, neuron, fired, motorNeuron, movement


    def PlotData(self):
        K, neuron, fired, motorNeuron, movement = self.TransformData()

        plt.subplot(511)
        plt.ylabel('Sensory   \n stimulus   ', fontsize=16, rotation=0)
        axes = plt.gca()
        axes.set_ylim([0,10])
        for ray in K:
            plt.plot(range(len(ray)), ray)

        plt.subplot(512)
        plt.ylabel('V1   ', fontsize=16, rotation=0)
        axes = plt.gca()
        axes.set_ylim([-100,20])
        plt.plot(range(len(neuron[0])), neuron[0], 'b')
        for i, element in enumerate(fired[0]):
            if element == 1:
                plt.axvline(x=i, linewidth=4, ymin=0, ymax=0.1, color='k')
        
        plt.subplot(513)
        plt.ylabel('V2   ', fontsize=16, rotation=0)
        axes = plt.gca()
        axes.set_ylim([-100,20])
        plt.plot(range(len(neuron[1])), neuron[1], 'b')
        for i, element in enumerate(fired[1]):
            if element == 1:
                plt.axvline(x=i, linewidth=4, ymin=0, ymax=0.1, color='k')
        
        plt.subplot(514)
        plt.ylabel('V3   ', fontsize=16, rotation=0)
        axes = plt.gca()
        axes.set_ylim([-100,20])
        plt.plot(range(len(neuron[2])), neuron[2], 'r') 
        for i, element in enumerate(fired[2]):
            if element == 1:
                plt.axvline(x=i, linewidth=4, ymin=0, ymax=0.1, color='k')

        plt.subplot(515)
        plt.xlabel('Time', fontsize=16)
        plt.plot(range(len(motorNeuron[0])), motorNeuron[0])
        plt.plot(range(len(motorNeuron[1])), motorNeuron[1])
        plt.plot(range(len(motorNeuron[0])), movement, 'k:')
        plt.ylabel('Movement   ', fontsize=16, rotation=0)
        plt.show()


def PlotGA(currentFile):
    df = pd.read_csv(currentFile)
    fitness = df.iloc[:,1]
    plt.plot([fitness[i] for i in range(0, len(fitness)-25000, 1000)], label='Fittest individual')
    plt.plot([np.mean(fitness[i:1000+i]) for i in range(0, len(fitness)-25000, 1000)], label='Mean')
    plt.xlabel('Generation', fontsize=20)
    plt.ylabel('Fitness', fontsize=20)
    plt.legend(bbox_to_anchor=(1, 1.05), loc='upper left', frameon=False)
    plt.show()

    Wi = df.iloc[:, 35:44]
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax1.set_title('Generation 1', fontsize=16)
    axes = plt.gca()
    axes.set_ylim([None, 5])
    plt.hist(Wi[:1000])
    plt.ylabel('Frequency', fontsize=16)
    ax2 = fig.add_subplot(122)
    ax2.set_title('Generation 160', fontsize=16)
    axes = plt.gca()
    axes.set_ylim([None, 5])
    plt.hist(Wi[-25000:-24000])
    fig.text(0.5, 0.04, 'Weight values', fontsize=16, ha='center', va='center')
    plt.show()


def LoadCustom(currentFile, i):
    df = pd.read_csv(currentFile)
    population = list() 
    Fitness = float(df.iloc[i][1])
    n = 3
    A = np.array(df.iloc[i][2:5])
    B = np.array(df.iloc[i][5:8])
    C = np.array(df.iloc[i][8:11])
    D = np.array(df.iloc[i][11:14])
    Ws = np.array([list(df.iloc[i][14:21]), list(df.iloc[i][21:28]), list(df.iloc[i][28:35])])
    Wi = np.array([list(df.iloc[i][35:38]), list(df.iloc[i][38:41]), list(df.iloc[i][41:44])])
    Wm = np.array([list(df.iloc[i][44:47]), list(df.iloc[i][47:50])])
    ts = float(np.array(df.iloc[i][50]))
    tm = float(np.array(df.iloc[i][51]))
    Bs = float(np.array(df.iloc[i][52]))
    Bi = np.array(df.iloc[i][52:54])
    Bm = np.array(df.iloc[i][54:56])

    population.append([Fitness, n, A, B, C, D, Ws, Wi, Wm, ts, tm, Bs, Bi, Bm])

    return population


def Transform(population):
    transform = [[] for i in range(14)]
    transform[1] = population[0][1]
    for individual in population:
        transform[0].append(individual[0])
        for i in range(2, 14):
            transform[i].append(individual[i])

    result = list()
    for element in transform:
        result.append(np.array(element))
    parameters = np.array(result)

    return parameters


def PlotFinalPositions(finalPositions):
    positions = range(-50, 50, 4)[:-1]
    offset = range(48)

    fig = plt.figure()

    ax1 = fig.add_subplot(121)
    ax1.set_title('Circle catching', fontsize=20)
    axes = plt.gca()
    axes.invert_yaxis()
    plt.ylabel('Test', fontsize=16)
    plt.yticks(np.arange(24), np.array([i+1 for i in offset]))
    for i, agent in enumerate(finalPositions[:24]):
        plt.plot([agent, positions[i]], [offset[i], offset[i]], 'k:')
    plt.scatter(finalPositions[:24], offset[:24], c='b', marker='o')
    plt.scatter(positions, offset[:24], c='g', marker='o', label='Circle')
    ax2 = fig.add_subplot(122)
    ax2.set_title('Line avoidance', fontsize=20)
    axes = plt.gca()
    axes.invert_yaxis()
    axes.axes.get_yaxis().set_visible(False) 
    for i, agent in enumerate(finalPositions[24:]):
        plt.plot([agent, positions[i]], [offset[i], offset[i]], 'k:')
    plt.scatter(finalPositions[24:], offset[:24], c='b', marker='o', label='Agent')
    plt.scatter(positions, offset[:24], c='r', marker='s', label='Line')
    plt.legend(bbox_to_anchor=(1, 1.05), loc='upper left', frameon=False)
    plt.show()

    for i, (agent1, agent2) in enumerate(zip(finalPositions[:24], finalPositions[24:])):
        plt.scatter(agent1, offset[i], c='g', marker='o') 
        plt.scatter(agent2, offset[i], c='r', marker='o')
        plt.plot([agent1, agent2], [offset[i], offset[i]], 'k:')
    plt.plot([(agent2+agent1)/2 for agent1, agent2 in zip(finalPositions[:24], finalPositions[24:])], offset[:24], c='b', marker='+')
    plt.axhspan(7.5, 14.5, facecolor='0.5', alpha=0.2)
    plt.axvline(x=0, color='k', linestyle='--')
    axes = plt.gca()
    plt.ylabel('Test', fontsize=16)
    plt.yticks(np.arange(24), np.array([i+1 for i in offset[:24]]))
    axes.invert_yaxis()
    plt.show()


def TransferEntropy(n, binSize, d, firingData):
    totalBins = 0
    totalCount = list()
    # Calculate posible combinations
    combinations = list()
    seriesNumber = [i for i in range(n)]
    for i in seriesNumber:
        for j in seriesNumber:
            combinations.append([i,j])
    # TE for each test
    for data in firingData:
        # Condense spikes in bins
        fired  = [[element[i] for element in data] for i in range(n)]
        spikeBins = [[] for i in range(n)]
        for j, spikes in enumerate(fired):
            for i in range(0, len(fired[0]), binSize):
                if 1 in spikes[i:i+binSize]:
                    spikeBins[j].append(1)
                else:
                    spikeBins[j].append(0)
        totalBins += len(spikeBins[0])
        # Calculate spikeBin time stamps
        spikePositions = list()
        for series in spikeBins:
            seriesPositions = 0
            seriesPositions = [i for i, element in enumerate(series) if element == 1]
            if seriesPositions:
                spikePositions.append(seriesPositions)
            else:
                spikePositions.append([float('NaN')])
        # Extract counts
        eventCount = np.array([[0.0 for k in range(4)] for j in range(len(combinations))])
        for c, target in enumerate(combinations):
            i, j = target
            # Events 1 and 2
            for t in spikePositions[j]:
                if not np.isnan(t) and (t+d-1 in spikePositions[i]) and (t+d in spikePositions[i]):
                    eventCount[c][0] += 1  
                if not np.isnan(t) and (t+d-1 in spikePositions[i]):
                    eventCount[c][1] += 1  
            # Events 3 and 4
            for t in spikePositions[i]:
                if (t >= d) and (t-1 in spikePositions[i]):
                    eventCount[c][2] += 1
                if not np.isnan(t):
                    eventCount[c][3] += 1
        totalCount.append(eventCount)
    # Calculate provabilites
    totalCount = np.array(totalCount)
    totalCountSum = np.sum(totalCount, axis=0)
    provabilites = np.array([[0.0 for j in range(3)] for i in range(len(combinations))])
    for c, spikeCount in enumerate(totalCountSum):
        if spikeCount[0] != 0:
            provabilites[c][0] = spikeCount[0] / (n * totalBins)
            provabilites[c][1] = spikeCount[0] / spikeCount[1] 
            provabilites[c][2] = spikeCount[2] / spikeCount[3] 
        else:
            provabilites[c] = [0, 0, 0]
    # Calculate TE
    #print('p matrix', provabilites)
    TE = np.array([[0.0 for j in range(len(combinations))] for i in range(48)])
    for test, testCount in enumerate(totalCount):
        for c, combi in enumerate(testCount):
            if combi[0] != 0:
                TE[test][c] = combi[0] * provabilites[c][0] * np.log2(provabilites[c][1] / provabilites[c][2])

    return TE


def PlotHeatMaps(density, densityCircles, densityLines):
    fig = plt.figure()
    # Total TE
    ax1 = fig.add_subplot(131)
    ax1.set_title('Network', fontsize=20)
    ax1.set_ylabel('Delay', fontsize=16)
    plt.imshow(density, cmap='Greens', origin='lower')
    plt.colorbar()
    print('Max connections: ', np.max(density))
    # Circle TE
    ax2 = fig.add_subplot(132)
    ax2.set_title('Circle catching', fontsize=20)
    plt.imshow(densityCircles, cmap='BuPu', origin='lower')
    plt.colorbar()
    axes = plt.gca()
    axes.axes.get_yaxis().set_visible(False) 
    print('Circle test, max connections: ', np.max(densityCircles))
    # Line TE
    ax3 = fig.add_subplot(133)
    ax3.set_title('Line avoidance', fontsize=20)
    plt.imshow(densityLines, cmap='BuPu', origin='lower')
    plt.colorbar()
    axes = plt.gca()
    axes.axes.get_yaxis().set_visible(False)
    print('Line test, max connections: ', np.max(densityLines))

    fig.text(0.5, 0.04, 'Message length', fontsize=16, ha='center', va='center')
    plt.show() 


def PlotTE(TE):
    positions = range(-50, 50, 4)[:-1]
    fig = plt.figure()
    # Plot circle TE
    circlesTE = np.transpose(np.absolute(TE[:24]))
    # N1
    ax1 = fig.add_subplot(321)
    ax1.set_title('Circle catching', fontsize=20)
    ax1.set_ylabel('Neuron 1', fontsize=16)
    for conection in circlesTE[:3]:
        plt.plot(positions, conection)
    axes = plt.gca()
    axes.set_ylim([-0.0002,None])
    axes.axes.get_xaxis().set_visible(False)
    # N2 
    ax3 = fig.add_subplot(323) 
    ax3.set_ylabel('Neuron 2', fontsize=16)
    for conection in circlesTE[3:6]:
        ax3.plot(positions, conection)
    axes = plt.gca()
    axes.set_ylim([-0.0002,None])
    axes.axes.get_xaxis().set_visible(False)
    # N3  
    ax5 = fig.add_subplot(325)
    ax5.set_ylabel('Neuron 3', fontsize=16)
    for conection in circlesTE[6:]:
        ax5.plot(positions, conection)
    axes = plt.gca()
    axes.set_ylim([-0.0002,None])
    # Plot line TE
    linesTE = np.transpose(np.absolute(TE[24:]))
    # N1
    ax2 = fig.add_subplot(322)
    ax2.set_title('Line avoidance', fontsize=20)
    for conection in linesTE[:3]:
        ax2.plot(positions, conection)
    axes = plt.gca()
    #axes.set_ylim([0,0.0025])
    axes.axes.get_xaxis().set_visible(False)
    # N2
    ax4 = fig.add_subplot(324)
    for conection in linesTE[3:6]:
        ax4.plot(positions, conection)
    axes = plt.gca()
    #axes.set_ylim([0,0.005])
    axes.axes.get_xaxis().set_visible(False)  
    # N3  
    ax6 = fig.add_subplot(326)
    for i, conection in enumerate(linesTE[6:]):
        ax6.plot(positions, conection, label='Conection to '+str(i+1))
    axes = plt.gca()
    #axes.set_ylim([0,0.035])
    plt.legend(bbox_to_anchor=(1.05, -0.15), loc='upper right', frameon=False)

    fig.text(0.06, 0.5, 'Transfer entropy', fontsize=20, ha='center', va='center', rotation='vertical')
    plt.show() 


n = 3
ne = 1
networks = 1
delta = 0.1
maxBinSize = 100
maxDelay = 30

# Plot GA
PlotGA('2-3_network(def2).csv')
# Initialise variables
population = LoadCustom('2-3_network(def2).csv', -999)
Simulation = SNN(Transform(population), networks)
positions = range(-50, 50, 4)
testFitness = list()
firingData = list()
finalPositions = list()
density = [[0.0 for j in range(maxDelay)] for i in range(maxBinSize)]
densityLines = [[0.0 for j in range(maxDelay)] for i in range(maxBinSize)]
densityCircles = [[0.0 for j in range(maxDelay)] for i in range(maxBinSize)]
conections = ['1-1','1-2','1-3','2-1','2-2','2-3','3-1','3-2','3-3']
# Run network for fittest individual
for testType in [True, False]:
    for i in range(24):
        Simulation.Reset()
        p, fired, pos = Simulation.Run(delta, positions[i], testType)
        testFitness.append(p)
        firingData.append(fired) 
        finalPositions.append(pos) 
testFitness = np.array(testFitness)
fitness = np.sum(testFitness, axis=0) / 48.0
fitnessMax = max(fitness)
print('Fitness: ', fitnessMax)
# Plot final positions
PlotFinalPositions(finalPositions)
for binSize in range(1, maxBinSize):
    for delay in range(1, maxDelay):
        TE = TransferEntropy(n, binSize, delay, firingData)
        density[binSize-1][delay-1] = np.count_nonzero(TE)
        densityCircles[binSize-1][delay-1] = np.count_nonzero(TE[:24])
        densityLines[binSize-1][delay-1] = np.count_nonzero(TE[24:])
        
# Plot heat maps 
PlotHeatMaps(density, densityCircles, densityLines)
# Print TE results 
TE = TransferEntropy(n, 23, 2, firingData)
print('TE matrix: \n', TE)
print(np.count_nonzero(TE))
# Plot TE 
PlotTE(TE)