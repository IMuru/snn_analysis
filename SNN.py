import numpy as np


class SNN(object):
    def __init__(self, parameters, networks):
        self.defaults = [parameters, networks]
        self.GPU_init = True
        fitness, n, A, B, C, D, Ws, Wi, Wm, ts, tm, Bs, Bi, Bm = parameters
        self.networks = networks
        self.n = n
        # Create sensory neurons
        # Parametrization 
        self.ts = 0.5*ts + 1.5
        self.Bs = Bs - 3
        self.SensoryOutput = np.array([[0.0 for j in range(7)] for i in range(networks)], dtype='float64')
        # Create spiking neurons
        # Parametrization of neurons' dynamics 
        self.A = 0.04*A + 0.06
        self.B = 0.025*B + 0.225
        self.C = 7.5*C - 57.5
        self.D = 3*D + 5
        # Parametrization of neurons' conections
        self.Ws = 50*Ws
        self.Wi = 50*Wi
        self.Bi = 4*Bi
        # Neurons' state variables
        self.I = np.array([[0.0 for j in range(self.n)] for i in range(networks)], dtype='float64')
        self.S = np.array([[0.0 for j in range(self.n)] for i in range(networks)], dtype='float64')
        self.V = np.array([[-65.0 for j in range(n)] for i in range(networks)], dtype='float64')
        self.U = self.B * self.V
        # Neurons' output
        self.NeuronOutput = np.array([[0.0 for j in range(self.n)] for i in range(networks)], dtype='float64') 
        # Create motor neurons
        # Parametrization
        self.tm = 0.5*tm + 1.5
        self.Wm = 50*Wm
        self.Bm = 4*Bm
        self.h = 5
        # Neuron's output
        self.history = np.array([[[0.0 for k in range(self.n)] for j in range(networks)] for i in range(self.h)], dtype='float64') 
        self.MotorOutput = np.array([[0.0 for j in range(2)] for i in range(networks)], dtype='float64') 

        # Enviroment initialization
        self.agentX = np.zeros(networks)
        self.rayEnd = np.array([213.0, 217.0, 219.0, 220.0, 219.0, 217.0,  213.0])
        self.rayEndSides = np.array([213.0, 217.0, 219.0, 219.0, 217.0,  213.0])
        self.theta = np.array([-0.2618, -0.1745, -0.0873, 0.0, 0.0873, 0.1745, 0.2618])
        self.thetaSides = np.array([-0.2618, -0.1745, -0.0873, 0.0873, 0.1745, 0.2618])
        self.testShape = np.array([0.0, 275.0])
        self.K = np.array([[0.0 for j in range(7)] for i in range(networks)], dtype='float64')
        self.finishFlag = False
        self.data = [[] for i in range(3)]
    

    def Reset(self):
        parameters, networks = self.defaults
        self.__init__(parameters, networks)


    def Update(self, delta, GPU_init):
        # Calculate sensory input layer
        self.SensoryOutput = self.SensoryOutput + delta*((-self.SensoryOutput + self.K) / self.ts[:,None])
        # Calculate hidden layer
        self.S = np.sum(self.Ws / (1 + np.exp(-(self.SensoryOutput + self.Bs[:,None])))[:,None], axis=2)
        self.I = np.sum(self.Wi * self.NeuronOutput[:,None], axis=2)
        self.V = self.V + delta * (0.04*self.V**2 + 5*self.V + 140 - self.U + self.S + self.I)
        self.U = self.U + delta * (self.A * (self.B*self.V - self.U))
        self.NeuronOutput = (self.V >= 30) * 1
        self.V = self.V * np.logical_not(self.NeuronOutput) + self.C * self.NeuronOutput
        self.U = self.U * np.logical_not(self.NeuronOutput) + (self.U + self.D) * self.NeuronOutput
        # Calculate output layer
        self.history = np.roll(self.history, 1, axis=0)
        self.history[0] = self.NeuronOutput
        movingAverage = np.sum(self.history, axis=0) / self.h
        self.MotorOutput = self.MotorOutput + delta * ((-self.MotorOutput + np.sum(self.Wm * movingAverage[:,None], axis=2)) / self.tm[:,None])
        self.MotorOutput = self.MotorOutput * (self.MotorOutput > 0) * 1
        
        # Update every agent's velocity and position
        MotorOutputT = np.transpose(self.MotorOutput)
        velocityX = MotorOutputT[0] - MotorOutputT[1]
        self.agentX = self.agentX + delta * velocityX
        rightLimit = (self.agentX > 50) * 1
        leftLimit = (self.agentX < -50) * 1
        self.agentX = self.agentX * np.logical_not(rightLimit) * np.logical_not(leftLimit)+ (50 * rightLimit) - (50 * leftLimit)
        # Update test shape position
        self.testShape[1] = self.testShape[1] - 3.0 * delta
        # Get collision points and check if the test shape has reached the botom
        if self.testType:
            self.CircleCollision()
            if (self.testShape[1] - 1) <= 0:
                self.finishFlag = True
        else:
            self.LineCollision()
            if (self.testShape[1] - 16) <= 0:
                self.finishFlag = True

  
    def Run(self, delta, initialPosition, testType):
        self.testType = testType
        self.testShape[0] = initialPosition

        while(not self.finishFlag):
            self.Update(delta, self.GPU_init)
            self.GPU_init = False

        d = np.abs(self.agentX - self.testShape[0])
        dNormalized = d * (d < 45) + (d >= 45) * 45
        dNormalized = dNormalized / 45.0
       
        if self.testType:
            p = 1 - dNormalized
        
        else:
            p = dNormalized

        return p


    def LineCollision(self):
        self.K = np.array([np.zeros(7) for i in range(self.networks)])
        # Calculate solution and images
        X = self.agentX[:, None] + self.theta * self.testShape[1]
        y = self.testShape[1]
        # Calculate distances to collision points 
        d = np.sqrt((X - self.agentX[:, None])**2 + y**2)
        # Generate mask based on constrains and filter valid results
        validCollisions = (self.rayEnd > y) * (X > self.testShape[0] - 15) * (X < self.testShape[0] + 15) * 1
        d = d * validCollisions
        d = d + 220 * (d == 0)
        self.K = 10 - (d / 22.0)
    

    def CircleCollision(self):
        self.K = np.array([np.zeros(7) for i in range(self.networks)])
        # Calculate ray 3
        X = self.agentX
        sqroot = 225 - (self.agentX - self.testShape[0])**2
        sqroot = sqroot * (sqroot > 0)
        Y = -np.sqrt(sqroot) + self.testShape[1]              
        # Calculate distances to collision point
        d3 = np.sqrt((X - self.agentX)**2 + Y**2)
        # Generate mask based on constrains and filter valid results
        validCollisions = (Y > 0) * (Y < self.rayEnd[3]) * 1
        d3 = d3 * validCollisions

        # ax^2 + bx + c = 0, where a, b and c are real numbers and a != 0
        constant = -self.agentX[:, None] / self.thetaSides - self.testShape[1]
        a = (1 + 1.0 / self.thetaSides**2)[None, :]
        b = 2 * (constant / self.thetaSides[None, :] - self.testShape[0])
        c = constant**2 - 225 + self.testShape[0]**2
        # calculate the discriminant
        disc = b**2 - 4*a*c
        # Calculate collision solutions
        disc = disc * (disc>0 * 1)
        X1 = (-b + np.sqrt(disc))/(2*a)
        X2 = (-b - np.sqrt(disc))/(2*a)
        # Calculate images based on line equation
        Y1 = (X1 - self.agentX[:, None]) / self.thetaSides[None, :]
        Y2 = (X2 - self.agentX[:, None]) / self.thetaSides[None, :]
        # Calculate distances to collision points
        d1 = np.sqrt((X1 - self.agentX[:, None])**2 + Y1**2)
        d2 = np.sqrt((X2 - self.agentX[:, None])**2 + Y2**2)
        # Generate masks based on constrains 
        validCollisions1 = (Y1 > 0) * (Y1 < self.rayEndSides[None, :]) * 1
        validCollisions2 = (Y2 > 0) * (Y2 < self.rayEndSides[None, :]) * 1
        # Filter valid results
        d1 = d1 * validCollisions1
        d2 = d2 * validCollisions2
        # Get minimum distaces
        conflict = validCollisions1 * validCollisions2
        noConflict = np.logical_not(conflict)
        dMin = np.minimum(d1 * conflict, d2 * conflict)
        d = dMin + d1 * noConflict + d2 * noConflict
        # Insert result for ray 3
        d = np.insert(d, [3], d3[:, None], axis=1)
        d = d + 220 * (d == 0)
        self.K = 10 - (d / 22.0)